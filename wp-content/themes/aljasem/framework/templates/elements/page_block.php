<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Page Block element
 */

if ( is_numeric( $id ) ) {
	$page_block = get_post( $id );
} else {
	return;
}

if ( $page_block AND $page_block->post_type == 'us_page_block' ) {
	$page_block_content = $page_block->post_content;
} else {
	return;
}

// Remove [us_page_block] and [us_post_content] shortcode to avoid recursion
$page_block_content = preg_replace( '~\[us_page_block.+?\]~', '', $page_block_content );
$page_block_content = preg_replace( '~\[us_post_content.+?\]~', '', $page_block_content );

// Remove [vc_row] and [vc_column] if set
if ( $remove_rows ) {
	$page_block_content = str_replace( array( '[vc_row]', '[/vc_row]', '[vc_column]', '[/vc_column]' ), '', $page_block_content );
	$page_block_content = preg_replace( '~\[vc_row (.+?)]~', '', $page_block_content );
	$page_block_content = preg_replace( '~\[vc_column (.+?)]~', '', $page_block_content );
}

// Apply filters to Page Block content and echoing it ouside of us_open_wp_query_context
echo apply_filters( 'us_page_block_the_content', $page_block_content );
