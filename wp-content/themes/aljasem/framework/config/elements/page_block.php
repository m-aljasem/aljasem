<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

// Get Page Blocks
$us_page_blocks_list = us_get_posts_titles_for( 'us_page_block' );

return array(
	'title' => __( 'Page Block', 'us' ),
	'icon' => 'far fa-square',
	'params' => array(

		'id' => array(
			'title' => __( 'Page Block', 'us' ),
			'type' => 'select',
			'options' => $us_page_blocks_list,
			'std' => '',
			'admin_label' => TRUE,
		),
		'remove_rows' => array(
			'switch_text' => __( 'Exclude Rows and Columns from selected Page Block', 'us' ),
			'type' => 'switch',
			'std' => FALSE,
		),

	),
);
