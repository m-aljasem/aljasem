<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Theme's Theme Options config
 *
 * @var $config array Framework-based theme options config
 *
 * @return array Changed config
 */

// Material design menu dropdown effect default
unset( $config['general']['fields']['rounded_corners'] );
unset( $config['general']['fields']['links_underline'] );
unset( $config['colors']['fields']['change_alt_content_colors_start'] );
unset( $config['colors']['fields']['h_colors_4'] );
unset( $config['colors']['fields']['color_alt_content_bg'] );
unset( $config['colors']['fields']['color_alt_content_bg_alt'] );
unset( $config['colors']['fields']['color_alt_content_border'] );
unset( $config['colors']['fields']['color_alt_content_heading'] );
unset( $config['colors']['fields']['color_alt_content_text'] );
unset( $config['colors']['fields']['color_alt_content_link'] );
unset( $config['colors']['fields']['color_alt_content_link_hover'] );
unset( $config['colors']['fields']['color_alt_content_primary'] );
unset( $config['colors']['fields']['color_alt_content_secondary'] );
unset( $config['colors']['fields']['color_alt_content_faded'] );
unset( $config['colors']['fields']['change_alt_content_colors_end'] );
unset( $config['woocommerce']['fields']['shop_listing_style']['options']['trendy'] );

return $config;
