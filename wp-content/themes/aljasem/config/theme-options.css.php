<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Generates and outputs theme options' generated styleshets
 *
 * @action Before the template: us_before_template:config/theme-options.css
 * @action After the template: us_after_template:config/theme-options.css
 */

global $us_template_directory_uri;

// Define if supported plugins are enabled
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
$with_shop = class_exists( 'woocommerce' );
?>

/* CSS paths which need to be absolute
   =============================================================================================================================== */
@font-face {
	font-family: 'Material Icons';
	font-style: normal;
	font-weight: 400;
	src: url("<?php echo $us_template_directory_uri ?>/fonts/material-icons.woff2") format('woff2'),
	url("<?php echo $us_template_directory_uri ?>/fonts/material-icons.woff") format('woff');
	}
.material-icons {
	font-family: 'Material Icons';
	font-weight: normal;
	font-style: normal;
	letter-spacing: normal;
	text-transform: none;
	display: inline-block;
	white-space: nowrap;
	word-wrap: normal;
	direction: ltr;
	font-feature-settings: 'liga';
	-moz-osx-font-smoothing: grayscale;
	-webkit-font-smoothing: antialiased;
	}
@font-face {
	font-family: 'Font Awesome 5 Brands';
	font-display: block;				 
	font-style: normal;
	font-weight: normal;
	src: url("<?php echo $us_template_directory_uri ?>/fonts/fa-brands-400.woff2") format("woff2"),
	url("<?php echo $us_template_directory_uri ?>/fonts/fa-brands-400.woff") format("woff");
	}
.fab {
	font-family: 'Font Awesome 5 Brands';
	}
@font-face {
	font-family: 'fontawesome';
	font-display: block;
	font-style: normal;
	font-weight: 400;
	src: url("<?php echo $us_template_directory_uri ?>/fonts/fa-regular-400.woff2") format("woff2"),
	url("<?php echo $us_template_directory_uri ?>/fonts/fa-regular-400.woff") format("woff");
	}
.far {
	font-family: 'fontawesome';
	font-weight: 400;
	}
@font-face {
	font-family: 'fontawesome';
	font-display: block;
	font-style: normal;
	font-weight: 900;
	src: url("<?php echo $us_template_directory_uri ?>/fonts/fa-solid-900.woff2") format("woff2"),
	url("<?php echo $us_template_directory_uri ?>/fonts/fa-solid-900.woff") format("woff");
	}
.fa,
.fas {
	font-family: 'fontawesome';
	font-weight: 900;
	}

.style_phone6-1 > div {
	background-image: url(<?php echo $us_template_directory_uri ?>/framework/img/phone-6-black-real.png);
	}
.style_phone6-2 > div {
	background-image: url(<?php echo $us_template_directory_uri ?>/framework/img/phone-6-white-real.png);
	}
.style_phone6-3 > div {
	background-image: url(<?php echo $us_template_directory_uri ?>/framework/img/phone-6-black-flat.png);
	}
.style_phone6-4 > div {
	background-image: url(<?php echo $us_template_directory_uri ?>/framework/img/phone-6-white-flat.png);
	}
/* Default icon Leaflet URLs */
.leaflet-default-icon-path {
	background-image: url(<?php echo $us_template_directory_uri ?>/css/vendor/images/marker-icon.png);
	}



/* Typography
   =============================================================================================================================== */
<?php

// Global Text
$css = 'html, .l-header .widget {';
$css .= us_get_font_css( 'body', TRUE );
$css .= 'font-size:' . us_get_option( 'body_fontsize' ) . ';';
$css .= 'line-height:' . us_get_option( 'body_lineheight' ) . ';';
$css .= '}';

// Uploaded Fonts
$uploaded_fonts = us_get_option( 'uploaded_fonts', array() );
if ( is_array( $uploaded_fonts ) AND count( $uploaded_fonts ) > 0 ) {
	foreach ( $uploaded_fonts as $uploaded_font ) {
		$files = explode( ',', $uploaded_font['files'] );
		$urls = array();
		foreach ( $files as $file ) {
			$url = wp_get_attachment_url( $file );
			if ( $url ) {
				$urls[] = 'url(' . esc_url( $url ) . ') format("' . pathinfo( $url, PATHINFO_EXTENSION ) . '")';
			}
		}
		if ( count( $urls ) ) {
			$css .= '@font-face {';
			$css .= 'font-display: swap;';
			$css .= 'font-style: normal;';
			$css .= 'font-family:"' . strip_tags( $uploaded_font['name'] ) . '";';
			$css .= 'font-weight:' . $uploaded_font['weight'] . ';';
			$css .= 'src:' . implode( ', ', $urls ) . ';';
			$css .= '}';
		}
	}
}

// Headings h1-h6
for ( $i = 1; $i <= 6; $i ++ ) {
	if ( $i == 4 ) { // set to some elements styles as <h4>
		if ( $with_shop ) {
			$css .= '.woocommerce #reviews h2, .woocommerce .related > h2, .woocommerce .upsells > h2, .woocommerce .cross-sells > h2,';
		}
		$css .= '.widgettitle, .comment-reply-title, h' . $i . '{';
	} else {
		$css .= 'h' . $i . '{';
	}
	$css .= us_get_font_css( 'h' . $i );
	$css .= 'font-weight:' . us_get_option( 'h' . $i . '_fontweight' ) . ';';
	$css .= 'font-size:' . us_get_option( 'h' . $i . '_fontsize' ) . ';';
	$css .= 'line-height:' . us_get_option( 'h' . $i . '_lineheight' ) . ';';
	$css .= 'letter-spacing:' . us_get_option( 'h' . $i . '_letterspacing' ) . ';';
	$css .= 'margin-bottom:' . us_get_option( 'h' . $i . '_bottom_indent' ) . ';';
	if ( is_array( us_get_option( 'h' . $i . '_transform' ) ) ) {
		if ( in_array( 'italic', us_get_option( 'h' . $i . '_transform' ) ) ) {
			$css .= 'font-style: italic;';
		}
		if ( in_array( 'uppercase', us_get_option( 'h' . $i . '_transform' ) ) ) {
			$css .= 'text-transform: uppercase;';
		}
	}
	$css .= '}';
}

echo strip_tags( $css );
?>
@media (max-width: 767px) {
	html {
		font-size: <?php echo us_get_option( 'body_fontsize_mobile' ) ?>;
		line-height: <?php echo us_get_option( 'body_lineheight_mobile' ) ?>;
	}
	h1 {
		font-size: <?php echo us_get_option( 'h1_fontsize_mobile' ) ?>;
	}
	h1.vc_custom_heading {
		font-size: <?php echo us_get_option( 'h1_fontsize_mobile' ) ?> !important;
	}
	h2 {
		font-size: <?php echo us_get_option( 'h2_fontsize_mobile' ) ?>;
	}
	h2.vc_custom_heading {
		font-size: <?php echo us_get_option( 'h2_fontsize_mobile' ) ?> !important;
	}
	h3 {
		font-size: <?php echo us_get_option( 'h3_fontsize_mobile' ) ?>;
	}
	h3.vc_custom_heading {
		font-size: <?php echo us_get_option( 'h3_fontsize_mobile' ) ?> !important;
	}
	h4,
	<?php if ( $with_shop ) { ?>
	.woocommerce #reviews h2,
	.woocommerce .related > h2,
	.woocommerce .upsells > h2,
	.woocommerce .cross-sells > h2,
	<?php } ?>
	.widgettitle,
	.comment-reply-title {
		font-size: <?php echo us_get_option( 'h4_fontsize_mobile' ) ?>;
	}
	h4.vc_custom_heading {
		font-size: <?php echo us_get_option( 'h4_fontsize_mobile' ) ?> !important;
	}
	h5 {
		font-size: <?php echo us_get_option( 'h5_fontsize_mobile' ) ?>;
	}
	h5.vc_custom_heading {
		font-size: <?php echo us_get_option( 'h5_fontsize_mobile' ) ?> !important;
	}
	h6 {
		font-size: <?php echo us_get_option( 'h6_fontsize_mobile' ) ?>;
	}
	h6.vc_custom_heading {
		font-size: <?php echo us_get_option( 'h6_fontsize_mobile' ) ?> !important;
	}
}



/* Site Layout
   =============================================================================================================================== */
<?php if ( us_get_option( 'body_bg_image' ) AND $body_bg_image = usof_get_image_src( us_get_option( 'body_bg_image' ) ) ): ?>
body {
	background-image: url(<?php echo $body_bg_image[0] ?>);
	background-attachment: <?php echo ( us_get_option( 'body_bg_image_attachment' ) ) ? 'scroll' : 'fixed'; ?>;
	background-position: <?php echo us_get_option( 'body_bg_image_position' ) ?>;
	background-repeat: <?php echo us_get_option( 'body_bg_image_repeat' ) ?>;
	background-size: <?php echo us_get_option( 'body_bg_image_size' ) ?>;
}
<?php endif; ?>
body,
.l-header.pos_fixed {
	min-width: <?php echo us_get_option( 'site_canvas_width' ) ?>;
	}
.l-canvas.type_boxed,
.l-canvas.type_boxed .l-subheader,
.l-canvas.type_boxed .l-section.type_sticky,
.l-canvas.type_boxed ~ .l-footer {
	max-width: <?php echo us_get_option( 'site_canvas_width' ) ?>;
	}
.l-subheader-h,
.l-main-h,
.l-section-h,
.l-main .aligncenter,
.w-tabs-section-content-h {
	max-width: <?php echo us_get_option( 'site_content_width' ) ?>;
	}

/* Limit width for centered images */
@media screen and (max-width: <?php echo ( intval( us_get_option( 'site_content_width' ) ) + intval( us_get_option( 'body_fontsize' ) ) * 5 ) ?>px) {
.l-main .aligncenter {
	max-width: calc(100vw - 5rem);
	}
}

.l-sidebar {
	width: <?php echo us_get_option( 'sidebar_width' ) ?>;
	}
.l-content {
	width: <?php echo us_get_option( 'content_width' ) ?>;
	}

<?php if ( us_get_option( 'row_height' ) == 'small' ): ?>
.l-sidebar { padding: 2rem 0; }
<?php endif; ?>

/* Columns width regarding Responsive Layout */
<?php if ( ! us_get_option( 'responsive_layout' ) ) { ?>
.vc_col-sm-1 { width: 8.3333%; }
.vc_col-sm-2 { width: 16.6666%; }
.vc_col-sm-1\/5 { width: 20%; }
.vc_col-sm-3 { width: 25%; }
.vc_col-sm-4 { width: 33.3333%; }
.vc_col-sm-2\/5 { width: 40%; }
.vc_col-sm-5 { width: 41.6666%; }
.vc_col-sm-6 { width: 50%; }
.vc_col-sm-7 { width: 58.3333%; }
.vc_col-sm-3\/5 { width: 60%; }
.vc_col-sm-8 { width: 66.6666%; }
.vc_col-sm-9 { width: 75%; }
.vc_col-sm-4\/5 { width: 80%; }
.vc_col-sm-10 { width: 83.3333%; }
.vc_col-sm-11 { width: 91.6666%; }
.vc_col-sm-12 { width: 100%; }
.vc_col-sm-offset-0 { margin-left: 0; }
.vc_col-sm-offset-1 { margin-left: 8.3333%; }
.vc_col-sm-offset-2 { margin-left: 16.6666%; }
.vc_col-sm-offset-1\/5 { margin-left: 20%; }
.vc_col-sm-offset-3 { margin-left: 25%; }
.vc_col-sm-offset-4 { margin-left: 33.3333%; }
.vc_col-sm-offset-2\/5 { margin-left: 40%; }
.vc_col-sm-offset-5 { margin-left: 41.6666%; }
.vc_col-sm-offset-6 { margin-left: 50%; }
.vc_col-sm-offset-7 { margin-left: 58.3333%; }
.vc_col-sm-offset-3\/5 { margin-left: 60%; }
.vc_col-sm-offset-8 { margin-left: 66.6666%; }
.vc_col-sm-offset-9 { margin-left: 75%; }
.vc_col-sm-offset-4\/5 { margin-left: 80%; }
.vc_col-sm-offset-10 { margin-left: 83.3333%; }
.vc_col-sm-offset-11 { margin-left: 91.6666%; }
.vc_col-sm-offset-12 { margin-left: 100%; }
<?php } else { ?>
@media (max-width: <?php echo ( intval( us_get_option( 'columns_stacking_width' ) ) - 1 ) ?>px) {
.g-cols.reversed {
	flex-direction: column-reverse;
	}
.g-cols > div:not([class*=" vc_col-"]) {
	width: 100%;
	margin: 0 0 1rem;
	}
.g-cols.type_boxes > div,
.g-cols.reversed > div:first-child,
.g-cols:not(.reversed) > div:last-child,
.g-cols > div.has-fill {
	margin-bottom: 0;
	}
.vc_wp_custommenu.layout_hor,
.align_center_xs,
.align_center_xs .w-socials {
	text-align: center;
	}
}
<?php }



/* Buttons Styles
   =============================================================================================================================== */
$btn_styles = us_get_option( 'buttons' );
$btn_styles = ( is_array( $btn_styles ) ) ? $btn_styles : array();

// Set Default Style for non-editable button elements
?>
.tribe-events-button,
button[type="submit"]:not(.w-btn),
input[type="submit"] {
<?php
if ( $btn_styles[0]['font'] != 'body' ) {
	echo us_get_font_css( $btn_styles[0]['font'] );
}
?>
text-transform: <?php echo ( in_array( 'uppercase', $btn_styles[0]['text_style'] ) ) ? 'uppercase' : 'none' ?>;
	font-style: <?php echo ( in_array( 'italic', $btn_styles[0]['text_style'] ) ) ? 'italic' : 'normal' ?>;
	font-weight: <?php echo $btn_styles[0]['font_weight'] ?>;
	letter-spacing: <?php echo $btn_styles[0]['letter_spacing'] ?>;
	border-radius: <?php echo $btn_styles[0]['border_radius'] ?>;
	padding: <?php echo $btn_styles[0]['height'] ?> <?php echo $btn_styles[0]['width'] ?>;
	box-shadow: 0 <?php echo floatval( $btn_styles[0]['shadow'] ) / 2 ?>em <?php echo $btn_styles[0]['shadow'] ?> rgba(0, 0, 0, 0.4);
	background-color: <?php echo ( ! empty( $btn_styles[0]['color_bg'] ) ) ? $btn_styles[0]['color_bg'] : 'transparent' ?>;
	border-color: <?php echo ( ! empty( $btn_styles[0]['color_border'] ) ) ? $btn_styles[0]['color_border'] : 'transparent' ?>;
	color: <?php echo ( ! empty( $btn_styles[0]['color_text'] ) ) ? $btn_styles[0]['color_text'] : 'inherit' ?> !important;
}
.tribe-events-button,
button[type="submit"]:not(.w-btn):before,
input[type="submit"] {
	border-width: <?php echo $btn_styles[0]['border_width'] ?>;
}
.no-touch .tribe-events-button:hover,
.no-touch button[type="submit"]:not(.w-btn):hover,
.no-touch input[type="submit"]:hover {
	box-shadow: 0 <?php echo floatval( $btn_styles[0]['shadow_hover'] ) / 2 ?>em <?php echo $btn_styles[0]['shadow_hover'] ?> rgba(0, 0, 0, 0.4);
	background-color: <?php echo ( ! empty( $btn_styles[0]['color_bg_hover'] ) ) ? $btn_styles[0]['color_bg_hover'] : 'transparent' ?>;
	border-color: <?php echo ( ! empty( $btn_styles[0]['color_border_hover'] ) ) ? $btn_styles[0]['color_border_hover'] : 'transparent' ?>;
	color: <?php echo ( ! empty( $btn_styles[0]['color_text_hover'] ) ) ? $btn_styles[0]['color_text_hover'] : 'inherit' ?> !important;
}
<?php

// Generate Buttons Styles
foreach ( $btn_styles as $btn_style ) {

	// Add CSS attributes for WooCommerce buttons
	if ( $with_shop AND us_get_option( 'shop_secondary_btn_style' ) == $btn_style['id'] ) { ?>
	.woocommerce .button,
	<?php }
	if ( $with_shop AND us_get_option( 'shop_primary_btn_style' ) == $btn_style['id'] ) { ?>
	.woocommerce .button.alt,
	.woocommerce .button.checkout,
	.woocommerce .button.add_to_cart_button,
	<?php } ?>
	.us-btn-style_<?php echo $btn_style['id'] ?> {
		<?php
		if ( ! empty( $btn_style['color_text'] ) ) {
			echo 'color:' . $btn_style['color_text'] . '!important;';
		}
		echo us_get_font_css( $btn_style['font'] );
		?>
		font-weight: <?php echo $btn_style['font_weight'] ?>;
		font-style: <?php echo in_array( 'italic', $btn_style['text_style'] ) ? 'italic' : 'normal' ?>;
		text-transform: <?php echo in_array( 'uppercase', $btn_style['text_style'] ) ? 'uppercase' : 'none' ?>;
		letter-spacing: <?php echo $btn_style['letter_spacing'] ?>;
		border-radius: <?php echo $btn_style['border_radius'] ?>;
		padding: <?php echo $btn_style['height'] ?> <?php echo $btn_style['width'] ?>;
		background-color: <?php echo ( ! empty( $btn_style['color_bg'] ) ) ? $btn_style['color_bg'] : 'transparent' ?>;
		border-color: <?php echo ( ! empty( $btn_style['color_border'] ) ) ? $btn_style['color_border'] : 'transparent' ?>;
		box-shadow: <?php echo ( ! empty( $btn_style['shadow'] ) ) ? ( '0 ' . floatval( $btn_style['shadow'] ) / 2 . 'em ' . $btn_style['shadow'] . ' rgba(0,0,0,0.4)' ) : 'none' ?>;
		}
	<?php if ( $with_shop AND us_get_option( 'shop_secondary_btn_style' ) == $btn_style['id'] ) { ?>
	.woocommerce .button:before,
	<?php }
	if ( $with_shop AND us_get_option( 'shop_primary_btn_style' ) == $btn_style['id'] ) { ?>
	.woocommerce .button.alt:before,
	.woocommerce .button.checkout:before,
	.woocommerce .button.add_to_cart_button:before,
	<?php } ?>
	.us-btn-style_<?php echo $btn_style['id'] ?>:before {
		border-width: <?php echo $btn_style['border_width'] ?>;
		}
	<?php if ( $with_shop AND us_get_option( 'shop_secondary_btn_style' ) == $btn_style['id'] ) { ?>
	.no-touch .woocommerce .button:hover,
	<?php }
	if ( $with_shop AND us_get_option( 'shop_primary_btn_style' ) == $btn_style['id'] ) { ?>
	.no-touch .woocommerce .button.alt:hover,
	.no-touch .woocommerce .button.checkout:hover,
	.no-touch .woocommerce .button.add_to_cart_button:hover,
	<?php } ?>
	.no-touch .us-btn-style_<?php echo $btn_style['id'] ?>:hover {
		<?php
		if ( ! empty( $btn_style['color_text_hover'] ) ) {
			echo 'color:' . $btn_style['color_text_hover'] . '!important;';
		}
		?>
		box-shadow: 0 <?php echo floatval( $btn_style['shadow_hover'] ) / 2 ?>em <?php echo $btn_style['shadow_hover'] ?> rgba(0,0,0,0.4);
		background-color: <?php echo ( ! empty( $btn_style['color_bg_hover'] ) ) ? $btn_style['color_bg_hover'] : 'transparent' ?>;
		border-color: <?php echo ( ! empty( $btn_style['color_border_hover'] ) ) ? $btn_style['color_border_hover'] : 'transparent' ?>;
		}
	<?php
	// "Slide" hover type
	if ( isset( $btn_style['hover'] ) AND $btn_style['hover'] == 'slide' ) { ?>
	.us-btn-style_<?php echo $btn_style['id'] ?> {
		overflow: hidden;
		}
	.us-btn-style_<?php echo $btn_style['id'] ?> > * {
		position: relative;
		z-index: 1;
		}
	.no-touch .us-btn-style_<?php echo $btn_style['id'] ?>:hover {
		background-color: <?php echo ( ! empty( $btn_style['color_bg'] ) AND ! empty( $btn_style['color_bg_hover'] ) ) ? $btn_style['color_bg'] : 'transparent' ?>;
		}
	.no-touch .us-btn-style_<?php echo $btn_style['id'] ?>:after {
		content: '';
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		height: 0;
		transition: height 0.3s;
		background-color: <?php echo ( ! empty( $btn_style['color_bg_hover'] ) ) ? $btn_style['color_bg_hover'] : 'transparent' ?>;
		}
	.no-touch .us-btn-style_<?php echo $btn_style['id'] ?>:hover:after {
		height: 100%;
		}
	<?php
	}
}

if ( us_get_option( 'keyboard_accessibility' ) ) { ?>
a:focus,
button:focus,
input[type="checkbox"]:focus + i,
input[type="submit"]:focus {
	outline: 2px dotted <?php echo us_get_option( 'color_content_primary' ) ?>;
	}
<?php } else { ?>
a,
button,
input[type="submit"],
.ui-slider-handle {
	outline: none !important;
	}
<?php } ?>

/* Back to top Button */
.w-header-show,
.w-toplink {
	background-color: <?php echo us_get_option( 'back_to_top_color' ) ?>;
	}



/* Colors
   =============================================================================================================================== */

body {
	background-color: <?php echo us_get_option( 'color_body_bg' ) ?>;
	-webkit-tap-highlight-color: <?php echo us_hex2rgba( us_get_option( 'color_content_primary' ), 0.2 ) ?>;
}

/*************************** Header Colors ***************************/

/* Top Header Area */
.l-subheader.at_top,
.l-subheader.at_top .w-dropdown-list,
.l-subheader.at_top .type_mobile .w-nav-list.level_1 {
	background-color: <?php echo us_get_option( 'color_header_top_bg' ) ?>;
}
.l-subheader.at_top,
.l-subheader.at_top .w-dropdown.opened,
.l-subheader.at_top .type_mobile .w-nav-list.level_1 {
	color: <?php echo us_get_option( 'color_header_top_text' ) ?>;
}
.no-touch .l-subheader.at_top a:hover,
.no-touch .l-header.bg_transparent .l-subheader.at_top .w-dropdown.opened a:hover {
	color: <?php echo us_get_option( 'color_header_top_text_hover' ) ?>;
}

/* Main Header Area */
.header_ver .l-header,
.l-subheader.at_middle,
.l-subheader.at_middle .w-dropdown-list,
.l-subheader.at_middle .type_mobile .w-nav-list.level_1 {
	background-color: <?php echo us_get_option( 'color_header_middle_bg' ) ?>;
}
.l-subheader.at_middle,
.l-subheader.at_middle .w-dropdown.opened,
.l-subheader.at_middle .type_mobile .w-nav-list.level_1 {
	color: <?php echo us_get_option( 'color_header_middle_text' ) ?>;
}
.no-touch .l-subheader.at_middle a:hover,
.no-touch .l-header.bg_transparent .l-subheader.at_middle .w-dropdown.opened a:hover {
	color: <?php echo us_get_option( 'color_header_middle_text_hover' ) ?>;
}

/* Bottom Header Area */
.l-subheader.at_bottom,
.l-subheader.at_bottom .w-dropdown-list,
.l-subheader.at_bottom .type_mobile .w-nav-list.level_1 {
	background-color: <?php echo us_get_option( 'color_header_bottom_bg' ) ?>;
}
.l-subheader.at_bottom,
.l-subheader.at_bottom .w-dropdown.opened,
.l-subheader.at_bottom .type_mobile .w-nav-list.level_1 {
	color: <?php echo us_get_option( 'color_header_bottom_text' ) ?>;
}
.no-touch .l-subheader.at_bottom a:hover,
.no-touch .l-header.bg_transparent .l-subheader.at_bottom .w-dropdown.opened a:hover {
	color: <?php echo us_get_option( 'color_header_bottom_text_hover' ) ?>;
}

/* Transparent Header Colors */
.l-header.bg_transparent:not(.sticky) .l-subheader {
	color: <?php echo us_get_option( 'color_header_transparent_text' ) ?>;
}
.no-touch .l-header.bg_transparent:not(.sticky) .w-text a:hover,
.no-touch .l-header.bg_transparent:not(.sticky) .w-html a:hover,
.no-touch .l-header.bg_transparent:not(.sticky) .w-dropdown a:hover,
.no-touch .l-header.bg_transparent:not(.sticky) .type_desktop .menu-item.level_1:hover > .w-nav-anchor {
	color: <?php echo us_get_option( 'color_header_transparent_text_hover' ) ?>;
}
.l-header.bg_transparent:not(.sticky) .w-nav-title:after {
	background-color: <?php echo us_get_option( 'color_header_transparent_text_hover' ) ?>;
}

/* Search Colors */
.w-search-form {
	background-color: <?php echo us_get_option( 'color_header_search_bg' ) ?>;
	color: <?php echo us_get_option( 'color_header_search_text' ) ?>;
}

.w-search.layout_fullscreen .w-search-background {
	background-color: <?php echo us_get_option( 'color_header_search_bg' ) ?>;
}

.w-search.layout_fullscreen input:focus + .w-form-row-field-bar:before,
.w-search.layout_fullscreen input:focus + .w-form-row-field-bar:after {
	background-color: <?php echo us_get_option( 'color_header_search_text' ) ?>;
}

/*************************** Header Menu Colors ***************************/

/* Menu Item on hover */
.menu-item.level_1 > .w-nav-anchor:focus,
.no-touch .menu-item.level_1.opened > .w-nav-anchor,
.no-touch .menu-item.level_1:hover > .w-nav-anchor {
	background-color: <?php echo us_get_option( 'color_menu_hover_bg' ) ?>;
	color: <?php echo us_get_option( 'color_menu_hover_text' ) ?>;
}
.w-nav-title:after {
	background-color: <?php echo us_get_option( 'color_menu_hover_text' ) ?>;
}

/* Active Menu Item */
.menu-item.level_1.current-menu-item > .w-nav-anchor,
.menu-item.level_1.current-menu-parent > .w-nav-anchor,
.menu-item.level_1.current-menu-ancestor > .w-nav-anchor {
	background-color: <?php echo us_get_option( 'color_menu_active_bg' ) ?>;
	color: <?php echo us_get_option( 'color_menu_active_text' ) ?>;
}

/* Active Menu Item in transparent header */
.l-header.bg_transparent:not(.sticky) .type_desktop .menu-item.level_1.current-menu-item > .w-nav-anchor,
.l-header.bg_transparent:not(.sticky) .type_desktop .menu-item.level_1.current-menu-ancestor > .w-nav-anchor {
	background-color: <?php echo us_get_option( 'color_menu_transparent_active_bg' ) ?>;
	color: <?php echo us_get_option( 'color_menu_transparent_active_text' ) ?>;
}

/* Dropdowns */
.w-nav-list:not(.level_1) {
	background-color: <?php echo us_get_option( 'color_drop_bg' ) ?>;
	color: <?php echo us_get_option( 'color_drop_text' ) ?>;
}

/* Dropdown Item on hover */
.no-touch .menu-item:not(.level_1) > .w-nav-anchor:focus,
.no-touch .menu-item:not(.level_1):hover > .w-nav-anchor {
	background-color: <?php echo us_get_option( 'color_drop_hover_bg' ) ?>;
	color: <?php echo us_get_option( 'color_drop_hover_text' ) ?>;
}

/* Dropdown Active Item */
.menu-item:not(.level_1).current-menu-item > .w-nav-anchor,
.menu-item:not(.level_1).current-menu-parent > .w-nav-anchor,
.menu-item:not(.level_1).current-menu-ancestor > .w-nav-anchor {
	background-color: <?php echo us_get_option( 'color_drop_active_bg' ) ?>;
	color: <?php echo us_get_option( 'color_drop_active_text' ) ?>;
}

/* Menu Button */
.btn.menu-item > a {
	background-color: <?php echo us_get_option( 'color_menu_button_bg' ) ?> !important;
	color: <?php echo us_get_option( 'color_menu_button_text' ) ?> !important;
}
.no-touch .btn.menu-item > a:hover {
	background-color: <?php echo us_get_option( 'color_menu_button_hover_bg' ) ?> !important;
	color: <?php echo us_get_option( 'color_menu_button_hover_text' ) ?> !important;
}

/*************************** Content Colors ***************************/

/* Background Color */
body.us_iframe,
.l-preloader,
.l-canvas,
.l-footer,
.l-popup-box-content,
.g-filters.style_1 .g-filters-item.active,
.l-section.color_alternate .g-filters.style_2 .g-filters-item.active,
.w-cart-dropdown,
.w-pricing.style_1 .w-pricing-item-h,
.w-person.layout_card,
.leaflet-popup-content-wrapper,
.leaflet-popup-tip,
.select2-dropdown,
.us-woo-shop_modern .product-h,
.no-touch .us-woo-shop_modern .product-meta,
.woocommerce #payment .payment_box,
.wpcf7-form-control-wrap.type_select:after {
	background-color: <?php echo us_get_option( 'color_content_bg' ) ?>;
}

.woocommerce #payment .payment_methods li > input:checked + label,
.woocommerce .blockUI.blockOverlay {
	background-color: <?php echo us_get_option( 'color_content_bg' ) ?> !important;
}

.w-iconbox.style_circle.color_contrast .w-iconbox-icon {
	color: <?php echo us_get_option( 'color_content_bg' ) ?>;
}

/* Alternate Background Color */
.l-section.color_alternate,
.l-canvas.sidebar_none .l-section.for_comments,
.w-actionbox.color_light,
.w-author,
.no-touch .pagination a.page-numbers:hover,
.g-filters.style_1,
.g-filters.style_2 .g-filters-item.active,
.w-form.for_protected,
.w-iconbox.style_circle.color_light .w-iconbox-icon,
.w-profile,
.w-pricing.style_1 .w-pricing-item-header,
.w-pricing.style_2 .w-pricing-item-h,
.w-progbar-bar,
.w-progbar.style_3 .w-progbar-bar:before,
.w-progbar.style_3 .w-progbar-bar-count,
.w-socials.style_solid .w-socials-item-link,
.w-tabs.layout_timeline .w-tabs-item,
.w-tabs.layout_timeline .w-tabs-section-header-h,
.widget_calendar #calendar_wrap,
.no-touch .l-main .widget_nav_menu a:hover,
.select2-selection__choice,
.woocommerce .login,
.woocommerce .track_order,
.woocommerce .checkout_coupon,
.woocommerce .lost_reset_password,
.woocommerce .register,
.woocommerce .comment-respond,
.woocommerce .cart_totals,
.no-touch .woocommerce .product-remove a:hover,
.woocommerce .checkout #order_review,
.woocommerce ul.order_details,
.widget_shopping_cart,
.smile-icon-timeline-wrap .timeline-wrapper .timeline-block,
.smile-icon-timeline-wrap .timeline-feature-item.feat-item {
	background-color: <?php echo us_get_option( 'color_content_bg_alt' ) ?>;
}

.timeline-wrapper .timeline-post-right .ult-timeline-arrow l,
.timeline-wrapper .timeline-post-left .ult-timeline-arrow l,
.timeline-feature-item.feat-item .ult-timeline-arrow l {
	border-color: <?php echo us_get_option( 'color_content_bg_alt' ) ?>;
}

/* Border Color */
hr,
td,
th,
input,
textarea,
select,
.l-section,
.vc_column_container,
.vc_column-inner,
.w-form-row-field input:focus,
.w-form-row-field textarea:focus,
.widget_search input[type="text"]:focus,
.w-comments .children,
.w-image,
.w-separator,
.w-sharing-item,
.w-tabs-list,
.w-tabs-section,
.l-main .widget_nav_menu .menu,
.l-main .widget_nav_menu .menu-item a,
.wpml-ls-legacy-dropdown a,
.wpml-ls-legacy-dropdown-click a,
.woocommerce .quantity.buttons_added input.qty,
.woocommerce .quantity.buttons_added .plus,
.woocommerce .quantity.buttons_added .minus,
.woocommerce-tabs .tabs,
.woocommerce .commentlist .comment-text,
.woocommerce .related,
.woocommerce .upsells,
.woocommerce .cross-sells,
.woocommerce ul.order_details li,
.select2-selection,
.smile-icon-timeline-wrap .timeline-line {
	border-color: <?php echo us_get_option( 'color_content_border' ) ?>;
}

.w-iconbox.style_default.color_light .w-iconbox-icon,
.w-separator,
.pagination .page-numbers {
	color: <?php echo us_get_option( 'color_content_border' ) ?>;
}

.no-touch .color_alternate .pagination a.page-numbers:hover,
.no-touch .woocommerce #payment .payment_methods li > label:hover,
.widget_price_filter .ui-slider:before {
	background-color: <?php echo us_get_option( 'color_content_border' ) ?>;
}

.w-socials.style_outlined .w-socials-item-link {
	box-shadow: 0 0 0 2px <?php echo us_get_option( 'color_content_border' ) ?> inset;
}

/* Heading Color */
h1, h2, h3, h4, h5, h6,
.w-counter.color_heading .w-counter-value {
	color: <?php echo us_get_option( 'color_content_heading' ) ?>;
}

.w-progbar.color_heading .w-progbar-bar-h {
	background-color: <?php echo us_get_option( 'color_content_heading' ) ?>;
}

<?php if ( us_get_option( 'h1_color' ) ) { ?>
h1 {
	color: <?php echo us_get_option( 'h1_color' ) ?>
}

<?php }
if ( us_get_option( 'h2_color' ) ) { ?>
h2 {
	color: <?php echo us_get_option( 'h2_color' ) ?>
}

<?php }
if ( us_get_option( 'h3_color' ) ) { ?>
h3 {
	color: <?php echo us_get_option( 'h3_color' ) ?>
}

<?php }
if ( us_get_option( 'h4_color' ) ) { ?>
h4 {
	color: <?php echo us_get_option( 'h4_color' ) ?>
}

<?php }
if ( us_get_option( 'h5_color' ) ) { ?>
h5 {
	color: <?php echo us_get_option( 'h5_color' ) ?>
}

<?php }
if ( us_get_option( 'h6_color' ) ) { ?>
h6 {
	color: <?php echo us_get_option( 'h6_color' ) ?>
}

<?php } ?>

/* Text Color */
.l-canvas,
.l-footer,
.l-popup-box-content,
.w-cart-dropdown,
.w-iconbox.style_circle.color_light .w-iconbox-icon,
.w-pricing-item-h,
.w-person.layout_card,
.w-tabs.layout_timeline .w-tabs-item,
.w-tabs.layout_timeline .w-tabs-section-header-h,
.leaflet-popup-content-wrapper,
.leaflet-popup-tip,
.woocommerce .form-row .chosen-drop,
.us-woo-shop_modern .product-h,
.select2-dropdown {
	color: <?php echo us_get_option( 'color_content_text' ) ?>;
}

.w-iconbox.style_circle.color_contrast .w-iconbox-icon,
.w-progbar.color_text .w-progbar-bar-h,
.w-scroller-dot span {
	background-color: <?php echo us_get_option( 'color_content_text' ) ?>;
}

.w-scroller-dot span {
	box-shadow: 0 0 0 2px<?php echo us_get_option( 'color_content_text' ) ?>;
}

/* Link Color */
a {
	color: <?php echo us_get_option( 'color_content_link' ) ?>;
}

/* Link Hover Color */
.no-touch a:hover {
	color: <?php echo us_get_option( 'color_content_link_hover' ) ?>;
}

.no-touch .w-cart-dropdown a:not(.button):hover {
	color: <?php echo us_get_option( 'color_content_link_hover' ) ?> !important;
}

/* Primary Color */
.g-preloader,
.w-counter.color_primary .w-counter-value,
.w-iconbox.style_default.color_primary .w-iconbox-icon,
.g-filters.style_1 .g-filters-item.active,
.g-filters.style_3 .g-filters-item.active,
.w-form-row.focused i,
.no-touch .w-sharing.type_simple.color_primary .w-sharing-item:hover .w-sharing-icon,
.w-separator.color_primary,
.w-tabs-item.active,
.w-tabs-section.active .w-tabs-section-header,
.l-main .widget_nav_menu .menu-item.current-menu-item > a,
.woocommerce-tabs .tabs li.active,
.woocommerce #payment .payment_methods li > input:checked + label,
input[type="radio"]:checked + .wpcf7-list-item-label:before,
input[type="checkbox"]:checked + .wpcf7-list-item-label:before,
.highlight_primary {
	color: <?php echo us_get_option( 'color_content_primary' ) ?>;
}

.l-section.color_primary,
.no-touch .post_navigation-item:hover .post_navigation-item-arrow,
.highlight_primary_bg,
.w-actionbox.color_primary,
.pagination .page-numbers.current,
.w-form-row.focused .w-form-row-field-bar:before,
.w-form-row.focused .w-form-row-field-bar:after,
.w-form-row input:checked + .w-form-checkbox,
.w-form-row input:checked + .w-form-radio,
.w-form-row.for_radio label > input:checked + i,
.no-touch .g-filters.style_1 .g-filters-item:hover,
.no-touch .g-filters.style_2 .g-filters-item:hover,
.w-grid-item-placeholder,
.w-post-elm.post_taxonomy.style_badge a,
.w-iconbox.style_circle.color_primary .w-iconbox-icon,
.w-pricing.style_1 .type_featured .w-pricing-item-header,
.w-pricing.style_2 .type_featured .w-pricing-item-h,
.w-progbar.color_primary .w-progbar-bar-h,
.w-sharing.type_solid.color_primary .w-sharing-item,
.w-sharing.type_fixed.color_primary .w-sharing-item,
.w-socials-item-link-hover,
.w-tabs-list-bar,
.w-tabs.layout_timeline .w-tabs-item.active,
.no-touch .w-tabs.layout_timeline .w-tabs-item:hover,
.w-tabs.layout_timeline .w-tabs-section.active .w-tabs-section-header-h,
.rsDefault .rsThumb.rsNavSelected,
.widget_price_filter .ui-slider-range,
.widget_price_filter .ui-slider-handle,
.select2-results__option--highlighted,
.smile-icon-timeline-wrap .timeline-separator-text .sep-text,
.smile-icon-timeline-wrap .timeline-wrapper .timeline-dot,
.smile-icon-timeline-wrap .timeline-feature-item .timeline-dot,
.l-body .cl-btn {
	background-color: <?php echo us_get_option( 'color_content_primary' ) ?>;
}

.l-content blockquote,
input:focus,
textarea:focus,
.w-separator.color_primary,
.owl-dot.active span,
.rsBullet.rsNavSelected span,
.woocommerce .quantity.buttons_added input.qty:focus,
.validate-required.woocommerce-validated input:focus,
.validate-required.woocommerce-invalid input:focus,
.woocommerce .form-row .chosen-search input[type="text"]:focus,
.woocommerce-tabs .tabs li.active {
	border-color: <?php echo us_get_option( 'color_content_primary' ) ?>;
}

input:focus,
textarea:focus {
	box-shadow: 0 -1px 0 0 <?php echo us_get_option( 'color_content_primary' ) ?> inset;
}

/* Secondary Color */
.highlight_secondary,
.w-counter.color_secondary .w-counter-value,
.w-iconbox.style_default.color_secondary .w-iconbox-icon,
.w-iconbox.style_default .w-iconbox-link:active .w-iconbox-icon,
.no-touch .w-iconbox.style_default .w-iconbox-link:hover .w-iconbox-icon,
.w-iconbox-link:active .w-iconbox-title,
.no-touch .w-iconbox-link:hover .w-iconbox-title,
.no-touch .w-sharing.type_simple.color_secondary .w-sharing-item:hover .w-sharing-icon,
.w-separator.color_secondary,
.star-rating span:before {
	color: <?php echo us_get_option( 'color_content_secondary' ) ?>;
}

.l-section.color_secondary,
.highlight_secondary_bg,
.w-actionbox.color_secondary,
.no-touch .w-post-elm.post_taxonomy.style_badge a:hover,
.w-iconbox.style_circle.color_secondary .w-iconbox-icon,
.w-progbar.color_secondary .w-progbar-bar-h,
.w-sharing.type_solid.color_secondary .w-sharing-item,
.w-sharing.type_fixed.color_secondary .w-sharing-item,
.no-touch .w-toplink.active:hover,
.no-touch .tp-leftarrow.tparrows.custom:hover,
.no-touch .tp-rightarrow.tparrows.custom:hover,
p.demo_store,
.woocommerce .onsale,
.woocommerce .form-row .chosen-results li.highlighted {
	background-color: <?php echo us_get_option( 'color_content_secondary' ) ?>;
}

.w-separator.color_secondary {
	border-color: <?php echo us_get_option( 'color_content_secondary' ) ?>;
}

/* Fade Elements Color */
blockquote:before,
.highlight_faded,
.w-form-row-description,
.l-main .w-author-url,
.l-main .post-author-website,
.l-main .w-profile-link.for_logout,
.l-main .g-tags,
.l-main .widget_tag_cloud,
.l-main .widget_product_tag_cloud {
	color: <?php echo us_get_option( 'color_content_faded' ) ?>;
}
.w-form-checkbox,
.w-form-radio {
	border-color: <?php echo us_get_option( 'color_content_faded' ) ?>;
}

/*************************** Top Footer colors ***************************/

/* Background Color */
.color_footer-top,
.color_footer-top .wpcf7-form-control-wrap.type_select:after {
	background-color: <?php echo us_get_option( 'color_subfooter_bg' ) ?>;
}

/* Alternate Background Color */
.color_footer-top .widget_shopping_cart,
.color_footer-top .w-socials.style_solid .w-socials-item-link {
	background-color: <?php echo us_get_option( 'color_subfooter_bg_alt' ) ?>;
}

/* Border Color */
.color_footer-top,
.color_footer-top *:not([class*="us-btn-style"]),
.color_footer-top .w-form-row input:focus,
.color_footer-top .w-form-row textarea:focus {
	border-color: <?php echo us_get_option( 'color_subfooter_border' ) ?>;
}

.color_footer-top .w-separator.color_border {
	color: <?php echo us_get_option( 'color_subfooter_border' ) ?>;
}

/* Text Color */
.color_footer-top {
	color: <?php echo us_get_option( 'color_subfooter_text' ) ?>;
}

/* Link Color */
.color_footer-top a {
	color: <?php echo us_get_option( 'color_subfooter_link' ) ?>;
}

/* Link Hover Color */
.no-touch .color_footer-top a:hover,
.color_footer-top .w-form-row.focused  i {
	color: <?php echo us_get_option( 'color_subfooter_link_hover' ) ?>;
}

.color_footer-top .w-form-row.focused .w-form-row-field-bar:before,
.color_footer-top .w-form-row.focused .w-form-row-field-bar:after {
	background-color: <?php echo us_get_option( 'color_subfooter_link_hover' ) ?>;
}

.color_footer-top input:focus,
.color_footer-top textarea:focus {
	border-color: <?php echo us_get_option( 'color_subfooter_link_hover' ) ?>;
	box-shadow: 0 -1px 0 0 <?php echo us_get_option( 'color_subfooter_link_hover' ) ?> inset;
}

/*************************** Bottom Footer Colors ***************************/

/* Background Color */
.color_footer-bottom,
.color_footer-bottom .wpcf7-form-control-wrap.type_select:after {
	background-color: <?php echo us_get_option( 'color_footer_bg' ) ?>;
}

/* Alternate Background Color */
.color_footer-bottom .widget_shopping_cart,
.color_footer-bottom .w-socials.style_solid .w-socials-item-link {
	background-color: <?php echo us_get_option( 'color_footer_bg_alt' ) ?>;
}

/* Border Color */
.color_footer-bottom,
.color_footer-bottom *:not([class*="us-btn-style"]),
.color_footer-bottom .w-form-row input:focus,
.color_footer-bottom .w-form-row textarea:focus {
	border-color: <?php echo us_get_option( 'color_footer_border' ) ?>;
}

.color_footer-bottom .w-separator.color_border {
	color: <?php echo us_get_option( 'color_footer_border' ) ?>;
}

/* Text Color */
.color_footer-bottom {
	color: <?php echo us_get_option( 'color_footer_text' ) ?>;
}

/* Link Color */
.color_footer-bottom a {
	color: <?php echo us_get_option( 'color_footer_link' ) ?>;
}

/* Link Hover Color */
.no-touch .color_footer-bottom a:hover,
.color_footer-bottom .w-form-row.focused i {
	color: <?php echo us_get_option( 'color_footer_link_hover' ) ?>;
}

.color_footer-bottom .w-form-row.focused .w-form-row-field-bar:before,
.color_footer-bottom .w-form-row.focused .w-form-row-field-bar:after {
	background-color: <?php echo us_get_option( 'color_footer_link_hover' ) ?>;
}

.color_footer-bottom input:focus,
.color_footer-bottom textarea:focus {
	border-color: <?php echo us_get_option( 'color_footer_link_hover' ) ?>;
	box-shadow: 0 -1px 0 0 <?php echo us_get_option( 'color_footer_link_hover' ) ?> inset;
}

/* Menu Dropdown Settings
   =============================================================================================================================== */
<?php
global $wpdb;
$wpdb_query = 'SELECT `id` FROM `' . $wpdb->posts . '` WHERE `post_type` = "nav_menu_item"';
$menu_items = array();
foreach ( $wpdb->get_results( $wpdb_query ) as $result ) {
	$menu_items[] = $result->id;
}
foreach ($menu_items as $menu_item_id):
	$settings = ( get_post_meta( $menu_item_id, 'us_mega_menu_settings', TRUE ) ) ? get_post_meta( $menu_item_id, 'us_mega_menu_settings', TRUE ) : array();
	if ( empty($settings) ) {continue;} ?>

<?php if ( $settings['columns'] != '1' AND $settings['width'] == 'full' ): ?>
.header_hor .w-nav.type_desktop .menu-item-<?php echo $menu_item_id; ?> {
	position: static;
}

.header_hor .w-nav.type_desktop .menu-item-<?php echo $menu_item_id; ?> .w-nav-list.level_2 {
	left: 0;
	right: 0;
	width: 100%;
	transform-origin: 50% 0;
}

.headerinpos_bottom .l-header.pos_fixed:not(.sticky) .w-nav.type_desktop .menu-item-<?php echo $menu_item_id; ?> .w-nav-list.level_2 {
	transform-origin: 50% 100%;
}

<?php endif; ?>

<?php if ( $settings['direction'] == 1 AND ( $settings['columns'] == '1' OR ( $settings['columns'] != '1' AND $settings['width'] == 'custom' ) ) ): ?>
.header_hor:not(.rtl) .w-nav.type_desktop .menu-item-<?php echo $menu_item_id; ?> .w-nav-list.level_2 {
	right: 0;
	transform-origin: 100% 0;
}

.header_hor.rtl .w-nav.type_desktop .menu-item-<?php echo $menu_item_id; ?> .w-nav-list.level_2 {
	left: 0;
	transform-origin: 0 0;
}

<?php endif; ?>

.w-nav.type_desktop .menu-item-<?php echo $menu_item_id; ?> .w-nav-list.level_2 {
	padding: <?php echo $settings['padding']; ?>;
	background-size: <?php echo $settings['bg_image_size']; ?>;
	background-repeat: <?php echo $settings['bg_image_repeat']; ?>;
	background-position: <?php echo $settings['bg_image_position']; ?>;

<?php if ( $settings['bg_image'] AND $bg_image = usof_get_image_src( $settings['bg_image'] ) ): ?> background-image: url(<?php echo $bg_image[0] ?>);
<?php endif;

if ( $settings['color_bg'] != '' ): ?> background-color: <?php echo $settings['color_bg']; ?>;
<?php endif;

if ( $settings['color_text'] != '' ): ?> color: <?php echo $settings['color_text']; ?>;
<?php endif;

if ( $settings['columns'] != '1' AND $settings['width'] == 'custom' ): ?> width: <?php echo $settings['custom_width']; ?>;
<?php endif; ?>

}

<?php endforeach; ?>
