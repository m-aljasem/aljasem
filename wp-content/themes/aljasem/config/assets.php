<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

unset( $config['material-icons'] );
unset( $config['gravityforms'] );
unset( $config['tribe-events'] );
unset( $config['bbpress'] );
unset( $config['tablepress'] );

return $config;
