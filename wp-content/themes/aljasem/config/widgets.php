<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Theme's Widgets config
 *
 * @var $config array Framework-based widgets config
 *
 * @return array Changed config
 */

unset( $config['us_socials']['params']['hover']['value']['slide'] );
unset( $config['us_contacts'] );

return $config;
