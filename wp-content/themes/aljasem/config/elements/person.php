<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

$config['params']['layout']['std'] = 'card';
$config['params']['layout']['options'] = array(
	'simple' => __( 'Simple', 'us' ),
	'simple_circle' => __( 'Simple (rounded photo)', 'us' ),
	'card' => __( 'Cards', 'us' ),
);

return $config;

