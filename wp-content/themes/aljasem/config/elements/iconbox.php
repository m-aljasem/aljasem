<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

$config['params']['style']['options'] = array(
	'default' => __( 'Simple', 'us' ),
	'circle' => __( 'Inside the Solid circle', 'us' ),
);

return $config;