<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

$style_param = array(
	'style' => array(
		'title' => us_translate( 'Style' ),
		'type' => 'select',
		'options' => array(
			'1' => __( 'Cards', 'us' ),
			'2' => __( 'Flat', 'us' ),
		),
		'std' => '1',
		'admin_label' => TRUE,
	),
);
$config['params'] = array_merge( $style_param, $config['params'] );

return $config;